//$Id$
package com.ant.sample.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

import com.ant.sample.Greetings;

/**
 * @author siva-4578
 *
 */
public class GreetingsTest {

	@Test
	public void testWelcomeMessageSuccess() {
		Greetings greetings = new Greetings();
		assertEquals("Hello there! Welcome to sample Apache Ant application.", greetings.welcome());
	}

    @Test
    public void testWelcomeMessageFailue() {
        Greetings greetings = new Greetings();
        assertNotEquals("Hello there! Welcome to sample Apache Ant application.", greetings.welcome());
    }

    @Test
    public void testWelcomeMessageFailue2() {
        Greetings greetings = new Greetings();
        assertEquals("Hello there! Welcome to sample Apache Ant application.", greetings.welcome());
    }
}
